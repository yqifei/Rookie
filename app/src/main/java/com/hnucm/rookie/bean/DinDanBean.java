package com.hnucm.rookie.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName DinDanBean
 * @Description 星光不问赶路人，时间不负有心人
 * @Author LuckyLazyPig
 * @Date 2022/3/13 22:44
 **/
@NoArgsConstructor
@Data
@AllArgsConstructor
public class DinDanBean {

    public String shopName;
    public String eatingStyle;
    public String time;
    public String diancan;
    public String price;
    public String judge;
    public String img;
}
