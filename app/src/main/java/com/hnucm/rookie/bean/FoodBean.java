package com.hnucm.rookie.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName FoodBean
 * @Description 星光不问赶路人，时间不负有心人
 * @Author LuckyLazyPig
 * @Date 2022/3/6 18:58
 **/
@NoArgsConstructor
@Data
@AllArgsConstructor
public class FoodBean {
    public String title;
    public String content;
    public String img;
}
