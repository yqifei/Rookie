package com.hnucm.rookie.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName PingJiaBean
 * @Description 星光不问赶路人，时间不负有心人
 * @Author LuckyLazyPig
 * @Date 2022/3/26 15:50
 **/
@NoArgsConstructor
@Data
@AllArgsConstructor
public class PingJiaBean {
    public String name;
    public String time;
    public String content;
    public String img1;
    public String img2;
}
