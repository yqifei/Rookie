package com.hnucm.rookie.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hnucm.rookie.R;
import com.hnucm.rookie.utils.RadiuImageView;

/**
 * @ClassName MyViewHolderShouYe
 * @Description 星光不问赶路人，时间不负有心人
 * @Author LuckyLazyPig
 * @Date 2022/3/6 10:22
 **/
public class MyViewHolderShouYe extends RecyclerView.ViewHolder {
    public RadiuImageView radiuImageView;
    public TextView textView;
    public TextView textView2;
    public MyViewHolderShouYe(@NonNull View itemView) {
        super(itemView);
        radiuImageView = itemView.findViewById(R.id.recycle_tuijian_img);
        textView = itemView.findViewById(R.id.textView42);
        textView2 = itemView.findViewById(R.id.textView43);
    }
}
