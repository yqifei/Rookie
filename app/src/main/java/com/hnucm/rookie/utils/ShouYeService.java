package com.hnucm.rookie.utils;

import com.hnucm.rookie.bean.FoodBean;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * @ClassName ShouYeService
 * @Description 星光不问赶路人，时间不负有心人
 * @Author LuckyLazyPig
 * @Date 2022/3/6 19:25
 **/
public interface ShouYeService {

    @GET("shouye/recly1")
    Call<List<FoodBean>> getFoodList();

}
