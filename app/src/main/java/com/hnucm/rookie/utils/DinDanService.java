package com.hnucm.rookie.utils;

import com.hnucm.rookie.bean.DinDanBean;
import com.hnucm.rookie.bean.PaiDuiBean;
import com.hnucm.rookie.bean.PingJiaBean;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * @ClassName DinDanService
 * @Description 星光不问赶路人，时间不负有心人
 * @Author LuckyLazyPig
 * @Date 2022/3/13 23:11
 **/
public interface DinDanService {
    @GET("dingdanall")
    Call<List<DinDanBean>> getDinDanAll();

    @GET("dingdanwait")
    Call<List<DinDanBean>> getDinDanWait();

    @GET("dingdanfinsh")
    Call<List<DinDanBean>> getDinDanFinish();

    @GET("pingjia")
    Call<List<PingJiaBean>> getPingJiaList();

    @GET("paidui")
    Call<List<PaiDuiBean>> getPaiDuiList();
}
