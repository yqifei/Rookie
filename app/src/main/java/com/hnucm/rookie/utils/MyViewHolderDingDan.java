package com.hnucm.rookie.utils;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.hnucm.rookie.R;
import com.google.android.material.button.MaterialButton;

/**
 * @ClassName MyViewHolderDingDan
 * @Description 星光不问赶路人，时间不负有心人
 * @Author LuckyLazyPig
 * @Date 2022/3/13 16:59
 **/
public class MyViewHolderDingDan extends RecyclerView.ViewHolder {
    public TextView status_pay_tv, shop_name_tv, jiucan_style_tv, time_tv, money_tv;
    public MaterialButton quxiao_pay_bt, zhifu_pay_bt;
    public ImageView status_pay_iv,shop_iv;
    public MyViewHolderDingDan(@NonNull View itemView) {
        super(itemView);
        status_pay_tv = itemView.findViewById(R.id.textView32);
        shop_name_tv = itemView.findViewById(R.id.textView33);
        jiucan_style_tv = itemView.findViewById(R.id.textView38);
        time_tv = itemView.findViewById(R.id.textView36);
        money_tv = itemView.findViewById(R.id.textView37);
        status_pay_iv = itemView.findViewById(R.id.imageView3);
        quxiao_pay_bt = itemView.findViewById(R.id.quxiao_mtbt);
        zhifu_pay_bt = itemView.findViewById(R.id.materialButton);
        shop_iv=itemView.findViewById(R.id.radiuImageView6);
    }
}
