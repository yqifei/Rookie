package com.hnucm.rookie.activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hnucm.rookie.R;
import com.hnucm.rookie.bean.PingJiaBean;
import com.hnucm.rookie.utils.DinDanService;
import com.hnucm.rookie.utils.RadiuImageView;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PinJiaMainActivity extends AppCompatActivity {

    @ViewInject(R.id.recycle_pinjia)
    private RecyclerView recyclerView;
    private MyAdapter myAdapter;
    private Retrofit retrofit;
    private DinDanService dinDanService;
    private ArrayList<PingJiaBean> pingJiaBeans = new ArrayList<>();
    
    
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setStatusBarColor(getResources().getColor(R.color.white));//设置状态栏颜色
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//实现状态栏图标和文字颜色为暗色
        setContentView(R.layout.activity_pin_jia_main);
        x.view().inject(this);
        initView();
        myAdapter = new MyAdapter();
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        request();
    }

    private void initView() {
        PingJiaBean luckyLazyPig = new PingJiaBean("LuckyLazyPig", "2022-03-22", "简直太好吃了，服务态度也超好，家人们可冲", "https://gitee.com/yqifei/img/raw/master/img/psc", "https://cdn.jsdelivr.net/gh/Yqifei/img@master/image.1ymukhdyc81s.webp");
        pingJiaBeans.add(luckyLazyPig);
    }

    private void request() {
        retrofit = new Retrofit.Builder()
                .baseUrl("https://www.fastmock.site/mock/ba5a8aedcc9048dcae31f29037db301a/rookie/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        dinDanService = retrofit.create(DinDanService.class);
        dinDanService.getPingJiaList().enqueue(new Callback<List<PingJiaBean>>() {
            @Override
            public void onResponse(Call<List<PingJiaBean>> call, Response<List<PingJiaBean>> response) {
                pingJiaBeans= (ArrayList<PingJiaBean>) response.body();
                for (PingJiaBean pingJiaBean : pingJiaBeans) {
                    System.out.println(pingJiaBean.toString());
                }
                myAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<PingJiaBean>> call, Throwable t) {

            }
        });
    }

    private class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView name,content,time;
        private RadiuImageView img1,img2;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.textView29);
            time=itemView.findViewById(R.id.textView_time);
            content=itemView.findViewById(R.id.textView30);
            img1=itemView.findViewById(R.id.radiuImageView5);
            img2=itemView.findViewById(R.id.radiuImageView10);
        }
    }

    private class MyAdapter extends RecyclerView.Adapter<MyViewHolder>{

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.pin_jia_item,parent,false);
            MyViewHolder myViewHolder = new MyViewHolder(view);
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            holder.time.setText(pingJiaBeans.get(position).time);
            holder.name.setText(pingJiaBeans.get(position).name);
            holder.content.setText(pingJiaBeans.get(position).content);
            Glide.with(getApplicationContext()).load(pingJiaBeans.get(position).img1).into(holder.img1);
            Glide.with(getApplicationContext()).load(pingJiaBeans.get(position).img2).into(holder.img2);
        }

        @Override
        public int getItemCount() {
            return pingJiaBeans.size();
        }
    }

}