package com.hnucm.rookie.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.hnucm.rookie.R;
import com.jaeger.library.StatusBarUtil;

public class ChongZhiActivity extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chong_zhi);
        StatusBarUtil.setColor(this,0x25A599,1);
        findViewById(R.id.imageView66).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChongZhiActivity.this,JiLuActivity.class));
            }
        });
    }
}