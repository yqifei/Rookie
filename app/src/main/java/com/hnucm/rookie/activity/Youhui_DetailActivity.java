package com.hnucm.rookie.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.hnucm.rookie.R;
import com.jaeger.library.StatusBarUtil;

public class Youhui_DetailActivity extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youhui__detail);
        StatusBarUtil.setTransparentForImageView(this,findViewById(R.id.constraintLayout30));
        AlertDialog alertDialog1 = new AlertDialog.Builder(this)
                .setTitle("恭喜你")//标题
                .setMessage("领取成功")//内容
                .setIcon(R.drawable.cheer)//图标
                .create();
        alertDialog1.show();
    }
}