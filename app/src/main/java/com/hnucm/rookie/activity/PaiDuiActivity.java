package com.hnucm.rookie.activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hnucm.rookie.R;
import com.hnucm.rookie.bean.PaiDuiBean;
import com.hnucm.rookie.fragment.GameFragment;
import com.hnucm.rookie.utils.DinDanService;
import com.hnucm.rookie.utils.RadiuImageView;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class PaiDuiActivity extends AppCompatActivity {

    @ViewInject(R.id.recyclerView)
    private RecyclerView recyclerView;
    private MyAdapter myAdapter = new MyAdapter();
    private Retrofit retrofit;
    private DinDanService dinDanService;
    private List<PaiDuiBean> paiDuiBeans=new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pai_dui);
        getWindow().setStatusBarColor(getResources().getColor(R.color.white));//设置状态栏颜色
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//实现状态栏图标和文字颜色为暗色
        x.view().inject(this);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        PaiDuiBean paiDuiBean = new PaiDuiBean();
        paiDuiBean.img="https://cdn.jsdelivr.net/gh/Yqifei/img@master/微信图片_20220409201413.6j5k2h68deo0.webp";
        paiDuiBean.name="草莓圣代";
        paiDuiBean.piece="10.99";
        paiDuiBeans.add(paiDuiBean);
        findViewById(R.id.materialButton10).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("id",2);
                startActivity(intent);
                finish();

            }
        });
        retrofit = new Retrofit.Builder()
                .baseUrl("https://www.fastmock.site/mock/ba5a8aedcc9048dcae31f29037db301a/rookie/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        dinDanService = retrofit.create(DinDanService.class);
        Call<List<PaiDuiBean>> paiDuiList = dinDanService.getPaiDuiList();
        paiDuiList.enqueue(new Callback<List<PaiDuiBean>>() {
            @Override
            public void onResponse(Call<List<PaiDuiBean>> call, Response<List<PaiDuiBean>> response) {
                paiDuiBeans = response.body();
                System.out.println(paiDuiBeans);
                myAdapter.notifyDataSetChanged();
                System.out.println("请求成功");
            }

            @Override
            public void onFailure(Call<List<PaiDuiBean>> call, Throwable t) {

            }
        });
    }

    private class MyViewHolder extends RecyclerView.ViewHolder{
        RadiuImageView radiuImageView;
        TextView textView_food,price_tv;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            radiuImageView = itemView.findViewById(R.id.radiuImageView4);
            textView_food = itemView.findViewById(R.id.textView21);
            price_tv = itemView.findViewById(R.id.price_tv);
        }
    }

    private class MyAdapter extends RecyclerView.Adapter<MyViewHolder>{

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.dingdan_item,parent,false);
            MyViewHolder myViewHolder = new MyViewHolder(view);
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

            Glide.with(getApplicationContext()).load(paiDuiBeans.get(position).img).into(holder.radiuImageView);

                holder.textView_food.setText(paiDuiBeans.get(position).name);
                holder.price_tv.setText("￥"+paiDuiBeans.get(position).piece);

        }

        @Override
        public int getItemCount() {
            return paiDuiBeans.size();
        }
    }
}