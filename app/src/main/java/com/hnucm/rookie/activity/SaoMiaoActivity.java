package com.hnucm.rookie.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.hnucm.rookie.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class SaoMiaoActivity extends AppCompatActivity {

    private ImageView ivImage;

    //  Step 1 : 初始化 获取控件 设置监听
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sao_miao);

        //获取测试的控件
        ivImage = findViewById(R.id.iv_image);//输出二维码图片

        //=======设置扫描活动  可根据需求设置以下内容
        IntentIntegrator intentIntegrator = new IntentIntegrator(SaoMiaoActivity.this);

        //  1.扫描成功后的提示音，默认关闭
        intentIntegrator.setBeepEnabled(true);

        //  2.启动后置摄像头扫描，若为 1 为前置摄像头，默认后置
        intentIntegrator.setCameraId(0);

        /*  3.设置扫描的条码的格式:默认为所有类型
         *   IntentIntegrator.PRODUCT_CODE_TYPES:商品码类型
         *   IntentIntegrator.ONE_D_CODE_TYPES:一维码类型
         *   IntentIntegrator.QR_CODE:二维码
         *   IntentIntegrator.DATA_MATRIX:数据矩阵类型
         *   IntentIntegrator.ALL_CODE_TYPES:所类有型
         * */
        intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);

                /*  4.方向锁：true为锁定，false反之，默认锁定.
                ps:在AndroidManifest.xml里设置以下属性，则扫码界面完全依赖传感器（tools红色提示，指向它会提示，点击左边蓝色Create...即可）
                <activity
                    android:name="com.journeyapps.barcodescanner.CaptureActivity"
                    android:screenOrientation="fullSensor"
                    tools:replace="screenOrientation" />
                * */
        intentIntegrator.setOrientationLocked(true);

        //  5.设置扫描界面的提示信息：默认为:请将条码置于取景框内扫描。(ps：设置没提示文字：setPrompt(""))
        intentIntegrator.setPrompt("请选择二维码");

        //  6.设置关闭扫描的时间(单位：毫秒)，不设置不关闭
        intentIntegrator.setTimeout(60000);

        //  7.保存二维码图片:在onActivityResult方法里可获取保存的路径，根据需要来是否需要保存
        intentIntegrator.setBarcodeImageEnabled(true);

        //启动扫描
        intentIntegrator.initiateScan();
    }




    //  Step 3 :处理扫码后返回的结果
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);

        if(result!=null){

            //==是否扫到内容
            if (result.getContents()!=null){
                Toast.makeText(this,"扫描结果："+result.getContents(),Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this,"取消扫码",Toast.LENGTH_LONG).show();
            }

            //==是否有保存照片的路径  在intentIntegrator已设置保存照片
            if(result.getBarcodeImagePath()!=null){

                FileInputStream file=null;
                try {
                    file=new FileInputStream(new File(result.getBarcodeImagePath()));
                    ivImage.setImageBitmap(BitmapFactory.decodeStream(file));//显示获取的照片
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }finally {
                    try {
                        file.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            /*  获取条码种类：在intentIntegrator.setDesiredBarcodeFormats那设置扫码格式后（点击格式可进入查看该格式有多少个类型）

                例如：PRODUCT_CODE_TYPES：商品码类型，它就有 UPC_A, UPC_E, EAN_8, EAN_13, RSS_14 种类
                public static final Collection<String> PRODUCT_CODE_TYPES = list(UPC_A, UPC_E, EAN_8, EAN_13, RSS_14);

                根据getFormatName获取到的种类，就知道是哪个扫码格式，进而根据需求进行相关操作
             */
            if (result.getFormatName()!=null){
                Toast.makeText(this,"图片格式："+result.getFormatName(),Toast.LENGTH_LONG).show();
            }


        }else{
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

}