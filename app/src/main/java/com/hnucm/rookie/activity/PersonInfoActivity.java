package com.hnucm.rookie.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.hnucm.rookie.R;
import com.hnucm.rookie.databinding.ActivityPersonInfoBinding;

public class PersonInfoActivity extends AppCompatActivity {

    private ActivityPersonInfoBinding activityPersonInfoBinding;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityPersonInfoBinding = ActivityPersonInfoBinding.inflate(getLayoutInflater());
        setContentView(activityPersonInfoBinding.getRoot());
        getWindow().setStatusBarColor(0xFFFFFF);//设置状态栏颜色
        getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//实现状态栏图标和文字颜色为暗色
        activityPersonInfoBinding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}