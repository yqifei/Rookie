package com.hnucm.rookie.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hnucm.rookie.R;
import com.hnucm.rookie.fragment.GameFragment;
import com.hnucm.rookie.fragment.TuiJianFragment;
import com.hnucm.rookie.fragment.XiaoXiFragment;
import com.hnucm.rookie.fragment.YongHuFragment;
import com.hnucm.rookie.fragment.ZhuShouFragment;

import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

public class MainActivity extends AppCompatActivity {

    private TuiJianFragment tuiJianFragment = new TuiJianFragment();
    private XiaoXiFragment xiaoXiFragment = new XiaoXiFragment();
    private YongHuFragment yongHuFragment = new YongHuFragment();
    private ZhuShouFragment zhuShouFragment = new ZhuShouFragment();
    private GameFragment gameFragment = new GameFragment();

    @ViewInject(R.id.linearLayout2)
    private LinearLayout linearLayout_tuijian;

    @ViewInject(R.id.linearLayout_xiaoyouxi)
    private LinearLayout linearLayout_xiaoyouxi;

    @ViewInject(R.id.linearLayout3)
    private LinearLayout linearLayout_zhushou;
    @ViewInject(R.id.linearLayout4)
    private LinearLayout linearLayout_xiaoxi;
    @ViewInject(R.id.linearLayout5)
    private LinearLayout linearLayout_yonghuzhongxin;

    @ViewInject(R.id.tuijian_tv)
    private TextView tuijianTv;
    @ViewInject(R.id.tuijian_iv)
    private ImageView tuijianIv;


    @ViewInject(R.id.xiaoxi_tv_xiaoyouxi)
    private TextView xiaoyouxiTv;
    @ViewInject(R.id.xiaoxi_iv_xiaoyouxi)
    private ImageView xiaoyouxiIv;

    @ViewInject(R.id.zhushou_tv)
    private TextView zhushouTv;
    @ViewInject(R.id.zhushou_iv)
    private ImageView zhushouIv;

    @ViewInject(R.id.xiaoxi_tv)
    private TextView xiaoxiTv;
    @ViewInject(R.id.xiaoxi_iv)
    private ImageView xiaoxiIv;

    @ViewInject(R.id.yonghuzhongxin_tv)
    private TextView yonghuzhongxinTv;
    @ViewInject(R.id.yonghuzhongxin_iv)
    private ImageView yonghuzhongxinIv;
    private Bundle args;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      //  getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);//隐藏状态栏但不隐藏状态栏字体
//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //隐藏状态栏，并且不显示字体
       // getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//实现状态栏文字颜色为暗色

      /*  getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        getWindow().setStatusBarColor(Color.TRANSPARENT);*/

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN );
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        args = new Bundle();
        args.putInt("index", 0);
        x.view().inject(this);
        initView();
        selectFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void selectFragment() {
        int id = getIntent().getIntExtra("id",0);
        if(id==2){
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN );
            getWindow().setStatusBarColor(Color.TRANSPARENT);
            tuijianIv.setSelected(false);
            tuijianTv.setSelected(false);
            xiaoyouxiIv.setSelected(false);
            xiaoyouxiTv.setSelected(false);
            zhushouTv.setSelected(true);
            zhushouIv.setSelected(true);
            xiaoxiIv.setSelected(false);
            xiaoxiTv.setSelected(false);
            yonghuzhongxinTv.setSelected(false);
            yonghuzhongxinIv.setSelected(false);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_replace,gameFragment).commit();
        }
    }

    private void initView(){
        tuijianIv.setSelected(true);
        tuijianTv.setSelected(true);
        zhushouTv.setSelected(false);
        zhushouIv.setSelected(false);
        xiaoxiIv.setSelected(false);
        xiaoxiTv.setSelected(false);
        xiaoyouxiIv.setSelected(false);
        xiaoyouxiTv.setSelected(false);
        yonghuzhongxinTv.setSelected(false);
        yonghuzhongxinIv.setSelected(false);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_replace,tuiJianFragment).commit();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Event(value = {R.id.linearLayout2,R.id.linearLayout3,R.id.linearLayout4,R.id.linearLayout5,R.id.linearLayout_xiaoyouxi},type = View.OnClickListener.class)
    private void clilkSelector(View view){
        switch (view.getId()){
            case R.id.linearLayout2:
            {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN );
                getWindow().setStatusBarColor(Color.TRANSPARENT);
                tuijianIv.setSelected(true);
                tuijianTv.setSelected(true);
                zhushouTv.setSelected(false);
                zhushouIv.setSelected(false);
                xiaoxiIv.setSelected(false);
                xiaoxiTv.setSelected(false);
                xiaoyouxiIv.setSelected(false);
                xiaoyouxiTv.setSelected(false);
                yonghuzhongxinTv.setSelected(false);
                yonghuzhongxinIv.setSelected(false);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_replace,tuiJianFragment).commit();
            }
            break;

            case R.id.linearLayout3:
            {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN );
                getWindow().setStatusBarColor(Color.TRANSPARENT);
                tuijianIv.setSelected(false);
                tuijianTv.setSelected(false);
                xiaoyouxiIv.setSelected(false);
                xiaoyouxiTv.setSelected(false);
                zhushouTv.setSelected(true);
                zhushouIv.setSelected(true);
                xiaoxiIv.setSelected(false);
                xiaoxiTv.setSelected(false);
                yonghuzhongxinTv.setSelected(false);
                yonghuzhongxinIv.setSelected(false);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_replace,gameFragment).commit();
            }
            break;
            case R.id.linearLayout4:
            {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN );
                getWindow().setStatusBarColor(Color.TRANSPARENT);
                tuijianIv.setSelected(false);
                tuijianTv.setSelected(false);
                zhushouTv.setSelected(false);
                zhushouIv.setSelected(false);
                xiaoyouxiIv.setSelected(false);
                xiaoyouxiTv.setSelected(false);
                xiaoxiIv.setSelected(true);
                xiaoxiTv.setSelected(true);
                yonghuzhongxinTv.setSelected(false);
                yonghuzhongxinIv.setSelected(false);
                xiaoXiFragment.setArguments(args);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_replace,xiaoXiFragment).commit();
            }
            break;
            case R.id.linearLayout5:
            {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN );
                getWindow().setStatusBarColor(Color.TRANSPARENT);
                tuijianIv.setSelected(false);
                tuijianTv.setSelected(false);
                zhushouTv.setSelected(false);
                zhushouIv.setSelected(false);
                xiaoxiIv.setSelected(false);
                xiaoyouxiIv.setSelected(false);
                xiaoyouxiTv.setSelected(false);
                xiaoxiTv.setSelected(false);
                yonghuzhongxinTv.setSelected(true);
                yonghuzhongxinIv.setSelected(true);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_replace,yongHuFragment).commit();
            }
            break;
            case R.id.linearLayout_xiaoyouxi:
            {
                tuijianIv.setSelected(false);
                tuijianTv.setSelected(false);
                zhushouTv.setSelected(false);
                zhushouIv.setSelected(false);
                xiaoxiIv.setSelected(false);
                xiaoyouxiIv.setSelected(true);
                xiaoyouxiTv.setSelected(true);
                xiaoxiTv.setSelected(false);
                yonghuzhongxinTv.setSelected(false);
                yonghuzhongxinIv.setSelected(false);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_replace,zhuShouFragment).commit();
            }
        }
    }
}