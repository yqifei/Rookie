package com.hnucm.rookie.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.hnucm.rookie.R;
import com.hnucm.rookie.activity.PersonInfoActivity;
import com.hnucm.rookie.activity.PinJiaMainActivity;
import com.hnucm.rookie.activity.ProblemFeedbackActivity;
import com.hnucm.rookie.activity.SettingActivity;
import com.hnucm.rookie.activity.TuiKuanActivity;
import com.hnucm.rookie.activity.VipMainActivity;
import com.hnucm.rookie.activity.YouHuiJuanActivity;

import org.xutils.view.annotation.ViewInject;

import java.util.List;

import lombok.ToString;


public class YongHuFragment extends Fragment {

    private View view;
    private ImageView imageViewSettings;
    private LinearLayout linearLayout;
    private RelativeLayout relativeLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment E1F0D6

        view = inflater.inflate(R.layout.fragment_yong_hu, container, false);
        initView();
        return view;
    }

    private void initView() {
        imageViewSettings = view.findViewById(R.id.setting_button);
        imageViewSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), SettingActivity.class));
            }
        });
        linearLayout = view.findViewById(R.id.person_info);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), PersonInfoActivity.class));
            }
        });
         relativeLayout = view.findViewById(R.id.problem_feedback);
         relativeLayout.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 startActivity(new Intent(getContext(), ProblemFeedbackActivity.class));
             }
         });
         view.findViewById(R.id.daifukuan_btn).setOnClickListener(new View.OnClickListener() {
             @SuppressLint("ResourceType")
             @Override
             public void onClick(View v) {
                 Bundle args = new Bundle();
                 args.putInt("index", 1);
                 XiaoXiFragment xiaoXiFragment = new XiaoXiFragment();
                 xiaoXiFragment.setArguments(args);
                 FragmentManager supportFragmentManager = getActivity().getSupportFragmentManager();
                 supportFragmentManager.beginTransaction().replace(R.id.fragment_replace,xiaoXiFragment).commit();
                 getActivity().findViewById(R.id.xiaoxi_iv).setSelected(true);
                 getActivity().findViewById(R.id.xiaoxi_tv).setSelected(true);
                 getActivity().findViewById(R.id.yonghuzhongxin_tv).setSelected(false);
                 getActivity().findViewById(R.id.yonghuzhongxin_iv).setSelected(false);
             }
         });
         view.findViewById(R.id.yifukuan_btn).setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Bundle args = new Bundle();
                 args.putInt("index", 2);
                 XiaoXiFragment xiaoXiFragment = new XiaoXiFragment();
                 xiaoXiFragment.setArguments(args);
                 FragmentManager supportFragmentManager = getActivity().getSupportFragmentManager();
                 supportFragmentManager.beginTransaction().replace(R.id.fragment_replace,xiaoXiFragment).commit();
                 getActivity().findViewById(R.id.xiaoxi_iv).setSelected(true);
                 getActivity().findViewById(R.id.xiaoxi_tv).setSelected(true);
                 getActivity().findViewById(R.id.yonghuzhongxin_tv).setSelected(false);
                 getActivity().findViewById(R.id.yonghuzhongxin_iv).setSelected(false);
             }
         });
         view.findViewById(R.id.daipingjia_btn).setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent intent = new Intent(getActivity(), PinJiaMainActivity.class);
                 startActivity(intent);
             }
         });
        view.findViewById(R.id.shouhou_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TuiKuanActivity.class);
                startActivity(intent);
            }
        });
        view.findViewById(R.id.linearLayout_suoyou).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putInt("index", 0);
                XiaoXiFragment xiaoXiFragment = new XiaoXiFragment();
                xiaoXiFragment.setArguments(args);
                FragmentManager supportFragmentManager = getActivity().getSupportFragmentManager();
                supportFragmentManager.beginTransaction().replace(R.id.fragment_replace,xiaoXiFragment).commit();
                getActivity().findViewById(R.id.xiaoxi_iv).setSelected(true);
                getActivity().findViewById(R.id.xiaoxi_tv).setSelected(true);
                getActivity().findViewById(R.id.yonghuzhongxin_tv).setSelected(false);
                getActivity().findViewById(R.id.yonghuzhongxin_iv).setSelected(false);
            }
        });
        view.findViewById(R.id.relativeLayout_huiyuan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), VipMainActivity.class);
                startActivity(intent);
            }
        });
        view.findViewById(R.id.xiaoyouxi_relative).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager supportFragmentManager = getActivity().getSupportFragmentManager();
                supportFragmentManager.beginTransaction().replace(R.id.fragment_replace,new GameFragment()).commit();
                getActivity().findViewById(R.id.zhushou_iv).setSelected(true);
                getActivity().findViewById(R.id.zhushou_tv).setSelected(true);
                getActivity().findViewById(R.id.yonghuzhongxin_tv).setSelected(false);
                getActivity().findViewById(R.id.yonghuzhongxin_iv).setSelected(false);
            }
        });
        view.findViewById(R.id.youhuiquan_relative).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), YouHuiJuanActivity.class);
                startActivity(intent);
            }
        });
    }
}