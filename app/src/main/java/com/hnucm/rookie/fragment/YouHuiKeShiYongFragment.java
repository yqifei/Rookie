package com.hnucm.rookie.fragment;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hnucm.rookie.R;
import com.hnucm.rookie.activity.YouHuiJuanActivity;


public class YouHuiKeShiYongFragment extends Fragment {

    private RecyclerView recyclerView;
    private  MyAdapter myAdapter;
    private View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_you_hui_ke_shi_yong, container, false);
        recyclerView=view.findViewById(R.id.recycle_youhui_quan);
        myAdapter = new MyAdapter();
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    private class MyViewHolder extends RecyclerView.ViewHolder{
        private View view;
        private TextView textViewManJian,textViewJinE,textViewLinQu;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView.findViewById(R.id.view2);
            textViewManJian=itemView.findViewById(R.id.tv_manjian);
            textViewJinE=itemView.findViewById(R.id.textView15);
            textViewLinQu=itemView.findViewById(R.id.tv_linqu);
        }
    }

    private class MyAdapter extends RecyclerView.Adapter<MyViewHolder>{

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view1 = LayoutInflater.from(getContext()).inflate(R.layout.youhui_quan_item,parent,false);
            MyViewHolder myViewHolder = new MyViewHolder(view1);
            return  myViewHolder;
        }

        @SuppressLint("ResourceAsColor")
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            if (position%3==0){
                holder.view.setBackground(getResources().getDrawable(R.color.youhui_quan_01));
                holder.textViewManJian.setTextColor(getResources().getColor(R.color.youhui_quan_01));
                holder.textViewJinE.setTextColor(getResources().getColor(R.color.youhui_quan_01));
                holder.textViewLinQu.setTextColor(getResources().getColor(R.color.youhui_quan_01));
            }else if(position%3==1){
                holder.view.setBackground(getResources().getDrawable(R.color.youhui_quan_02));
                holder.textViewManJian.setTextColor(getResources().getColor(R.color.youhui_quan_02));
                holder.textViewJinE.setTextColor(getResources().getColor(R.color.youhui_quan_02));
                holder.textViewLinQu.setTextColor(getResources().getColor(R.color.youhui_quan_02));
            }else{
                holder.view.setBackground(getResources().getDrawable(R.color.youhui_quan_03));
                holder.textViewManJian.setTextColor(getResources().getColor(R.color.youhui_quan_03));
                holder.textViewJinE.setTextColor(getResources().getColor(R.color.youhui_quan_03));
                holder.textViewLinQu.setTextColor(getResources().getColor(R.color.youhui_quan_03));
            }
        }

        @Override
        public int getItemCount() {
            return 6;
        }
    }
}