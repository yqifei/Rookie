package com.hnucm.rookie.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.hnucm.rookie.R;
import com.hnucm.rookie.databinding.FragmentGameBinding;
import com.unity3d.player.UnityPlayerActivity;
import com.youth.banner.adapter.BannerImageAdapter;
import com.youth.banner.holder.BannerImageHolder;
import com.youth.banner.indicator.CircleIndicator;

import java.util.ArrayList;
import java.util.List;

public class GameFragment extends Fragment {
    List<String> img = new ArrayList<>();

    private FragmentGameBinding gameBinding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        gameBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_game,container,false);
        View view = gameBinding.getRoot();
        initView();
        return view;
    }

    private void initView() {
        if (img.size()==0){
            img.add("https://cdn.jsdelivr.net/gh/Yqifei/img@master/微信图片_202203291502512.25sx8lagl30g.webp");
            img.add("https://cdn.jsdelivr.net/gh/Yqifei/img@master/微信图片_20220329150251.6s53ysc6pe00.webp");
            img.add("https://cdn.jsdelivr.net/gh/Yqifei/img@master/微信图片_202203291502511.4szv4e0q2w40.webp");
            img.add("https://cdn.jsdelivr.net/gh/Yqifei/img@master/微信图片_202203291502513.445wr3t6ex20.webp");
        }
        gameBinding.banner1.setAdapter(new BannerImageAdapter<String>(img) {
            @Override
            public void onBindView(BannerImageHolder holder, String data, int position, int size) {
                System.out.println("hello TEST");
                Glide.with(holder.itemView)
                        .load(data)
                        .apply(RequestOptions.bitmapTransform(new RoundedCorners(30)))
                        .into(holder.imageView);
            }
        }).setIndicator(new CircleIndicator(getContext())).setLoopTime(1000).setIndicatorSpace(40).setIndicatorWidth(25,25);
        gameBinding.startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UnityPlayerActivity.class);
                startActivity(intent);
            }
        });
    }
}