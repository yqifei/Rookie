package com.hnucm.rookie.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hnucm.rookie.R;
import com.hnucm.rookie.databinding.FragmentDinDanAllBinding;
import com.hnucm.rookie.databinding.FragmentXiaoXiBinding;
import com.hnucm.rookie.utils.MyViewHolderDingDan;


public class DinDanAllFragment extends Fragment {

    private FragmentDinDanAllBinding dinDanAllBinding;
    private MyViewHolderDingDan myViewHolderDingDan;
    private RecyclerView recyclerView;
    private MyAdapter myAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        dinDanAllBinding = FragmentDinDanAllBinding.inflate(inflater,container,false);
        initView();
        return dinDanAllBinding.getRoot();
    }

    private void initView() {
        recyclerView = dinDanAllBinding.dingdanRecycler;
        myAdapter = new MyAdapter();
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private class MyAdapter extends RecyclerView.Adapter<MyViewHolderDingDan>{

        @NonNull
        @Override
        public MyViewHolderDingDan onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view1 = LayoutInflater.from(getContext()).inflate(R.layout.dingdan_status_item,parent,false);
            MyViewHolderDingDan myViewHolder = new MyViewHolderDingDan(view1);
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolderDingDan holder, int position) {
            if (position%3==1){
                holder.status_pay_tv.setText("已付款");
                holder.status_pay_tv.setTextColor(getResources().getColor(R.color.price_red));
                holder.quxiao_pay_bt.setText("订单回持");
                holder.status_pay_iv.setImageDrawable(getResources().getDrawable(R.drawable.hot_red));
                holder.shop_iv.setImageDrawable(getResources().getDrawable(R.drawable.canding2));
            }
            if (position%3==2){
                holder.status_pay_tv.setText("已完成");
                holder.status_pay_tv.setTextColor(getResources().getColor(R.color.blue_color));
                holder.quxiao_pay_bt.setVisibility(View.INVISIBLE);
                holder.zhifu_pay_bt.setText("立即评价");
                holder.status_pay_iv.setImageDrawable(getResources().getDrawable(R.drawable.hot_lan));
                holder.shop_iv.setImageDrawable(getResources().getDrawable(R.drawable.canding2));
            }
        }

        @Override
        public int getItemCount() {
            return 3;
        }
    }
}