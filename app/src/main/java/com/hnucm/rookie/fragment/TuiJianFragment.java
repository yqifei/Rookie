package com.hnucm.rookie.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.hnucm.rookie.R;
import com.hnucm.rookie.activity.PaiDuiActivity;
import com.hnucm.rookie.activity.ScanActivity;
import com.hnucm.rookie.activity.VipMainActivity;
import com.hnucm.rookie.activity.YouHuiJuanActivity;
import com.hnucm.rookie.adapter.MyViewHolderShouYe;
import com.hnucm.rookie.bean.FoodBean;
import com.hnucm.rookie.databinding.FragmentTuiJianBinding;
import com.hnucm.rookie.utils.ShouYeService;
import com.youth.banner.adapter.BannerImageAdapter;
import com.youth.banner.holder.BannerImageHolder;
import com.youth.banner.indicator.CircleIndicator;

import com.youth.banner.transformer.ScaleInTransformer;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class TuiJianFragment extends Fragment {



    private FragmentTuiJianBinding binding;
    private ArrayList<String> arrayList;
    private MyAdpater myAdpater;
    private Retrofit retrofit;
    private ShouYeService shouYeService;
    private List<FoodBean> beanList1;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentTuiJianBinding.inflate(inflater,container,false);
        FoodBean foodBean1 =  new FoodBean("夏日清凉一下","夏日清凉一下，带给你美味享受","https://cdn.jsdelivr.net/gh/shanshan911/image-hosting@master/image/87123cd002bfbe3814d2ab03274f560b.7f0kbr13jj80.webp");
        beanList1 = new ArrayList<>();
        beanList1.add(foodBean1);

        retrofit = new Retrofit.Builder()
                .baseUrl("https://www.fastmock.site/mock/ba5a8aedcc9048dcae31f29037db301a/rookie/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        shouYeService = retrofit.create(ShouYeService.class);
        initView();
        click();
        requset();
        return binding.getRoot();
    }

    private void click() {
        binding.paiduiLinghao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), PaiDuiActivity.class));
                getActivity().finish();
            }
        });
        binding.arscan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ScanActivity.class));
            }
        });
        binding.problemFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), VipMainActivity.class));
            }
        });
        binding.youhuiquan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), YouHuiJuanActivity.class));
            }
        });
    }


    //get异步请求
    private void requset() {
        Call<List<FoodBean>> listCall = shouYeService.getFoodList();
        listCall.enqueue(new Callback<List<FoodBean>>() {
            @Override
            public void onResponse(Call<List<FoodBean>> call, Response<List<FoodBean>> response) {
                beanList1 = response.body();
                System.out.println(response.body());
                myAdpater.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<FoodBean>> call, Throwable t) {
                System.out.println("请求失败");
            }
        });
    }

    @SuppressLint("ResourceType")
    private void initView() {
        arrayList = new ArrayList<>();
        arrayList.add("https://cdn.jsdelivr.net/gh/Yqifei/img@master/banner1.qw444u51df4.webp");
        arrayList.add("https://cdn.jsdelivr.net/gh/Yqifei/img@master/banner2.2xbo1u7d8uw0.webp");
        arrayList.add("https://cdn.jsdelivr.net/gh/Yqifei/img@master/banner3.1ypb6oz3zv40.webp");

        binding.banner.setAdapter(new BannerImageAdapter<String>(arrayList) {
            @Override
            public void onBindView(BannerImageHolder holder, String data, int position, int size) {
                Glide.with(holder.itemView)
                        .load(data)
                        .apply(RequestOptions.bitmapTransform(new RoundedCorners(100)))
                        .into(holder.imageView);
            }

        });
        binding.banner
                .setIndicator(new CircleIndicator(getContext())).isAutoLoop(true).setBannerRound(30)
                .setPageTransformer(new ScaleInTransformer())
                .setLoopTime(1000)
                .setScrollTime(1000);
        myAdpater = new MyAdpater();
        binding.recycleShouye1.setAdapter(myAdpater);
        binding.recycleShouye1.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
    }

    private class MyAdpater extends RecyclerView.Adapter<MyViewHolderShouYe>{

        @NonNull
        @Override
        public MyViewHolderShouYe onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.shouye_recly_item,parent,false);
            MyViewHolderShouYe myViewHolderShouYe = new MyViewHolderShouYe(view);
            return myViewHolderShouYe;
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolderShouYe holder, int position) {

            holder.textView.setText(beanList1.get(position).title);
            holder.textView2.setText(beanList1.get(position).content);
            System.out.println(beanList1.get(position).img);
            Glide.with(getContext()).load(beanList1.get(position).img).into(holder.radiuImageView);

        }

        @Override
        public int getItemCount() {
            return beanList1.size();
        }
    }


}