package com.hnucm.rookie.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.hnucm.rookie.R;
import com.hnucm.rookie.activity.MainActivity;
import com.hnucm.rookie.activity.ScanActivity;


public class ZhuShouFragment extends Fragment {

    private MaterialButton button;
    private View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_zhu_shou, container, false);
        initView();
        return view;
    }

    private void initView() {
        button = view.findViewById(R.id.materialButton1);
        listenerView();
    }

    private void listenerView() {
        //  Step 2 :跳转到扫描活动
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //=======设置扫描活动  可根据需求设置以下内容
                IntentIntegrator intentIntegrator = new IntentIntegrator(getActivity());

                //启动自定义的扫描活动，不设置则启动默认的活动
                intentIntegrator.setCaptureActivity(ScanActivity.class);

                //启动扫描
                intentIntegrator.initiateScan();

            }
        });
    }



    //  Step 3 :处理扫码后返回的结果
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);

        if(result!=null){

            //==是否扫到内容
            if (result.getContents()!=null){
                Toast.makeText(getContext(),"扫描结果："+result.getContents(),Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getContext(),"取消扫码",Toast.LENGTH_LONG).show();
            }


        }else{
            super.onActivityResult(requestCode, resultCode, data);
        }

    }



}