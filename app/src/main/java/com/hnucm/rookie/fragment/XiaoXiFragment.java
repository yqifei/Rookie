package com.hnucm.rookie.fragment;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.hnucm.rookie.R;
import com.hnucm.rookie.adapter.MyViewPageAdapter;
import com.hnucm.rookie.bean.DinDanBean;
import com.hnucm.rookie.databinding.FragmentTuiJianBinding;
import com.hnucm.rookie.databinding.FragmentXiaoXiBinding;
import com.hnucm.rookie.utils.DinDanService;
import com.hnucm.rookie.utils.MyViewHolderDingDan;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.hnucm.rookie.databinding.FragmentZhuShouBinding.inflate;


public class XiaoXiFragment extends Fragment {

    private FragmentXiaoXiBinding xiaoXiBinding;
    private View view;
    private String[] arrayTab = {"全部", "待付款", "已完成"};
    private MyViewHolderDingDan myViewHolderDingDan;
    private RecyclerView recyclerView;
    private MyAdapter myAdapter;
    private Retrofit retrofit;
    private DinDanService dinDanService;
    private List<DinDanBean> dinDanBeanList = new ArrayList<>();
    private int index;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        xiaoXiBinding = FragmentXiaoXiBinding.inflate(inflater, container, false);
        retrofit = new Retrofit.Builder()
                .baseUrl("https://www.fastmock.site/mock/ba5a8aedcc9048dcae31f29037db301a/rookie/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        dinDanService = retrofit.create(DinDanService.class);
        initView();
        addRecycleView();
        setIndex();
        return xiaoXiBinding.getRoot();
    }

    private void setIndex() {
        index = getArguments().getInt("index");
        xiaoXiBinding.mTabLayout.selectTab(xiaoXiBinding.mTabLayout.getTabAt(index));
    }

    private void addRecycleView() {
        recyclerView = xiaoXiBinding.dingdanRecycler;
        myAdapter = new MyAdapter();
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        request();
    }


    private void request() {
        Call<List<DinDanBean>> listCall = dinDanService.getDinDanAll();
        listCall.enqueue(new Callback<List<DinDanBean>>() {
            @Override
            public void onResponse(Call<List<DinDanBean>> call, Response<List<DinDanBean>> response) {
                dinDanBeanList = response.body();
                System.out.println(dinDanBeanList);
                myAdapter.notifyDataSetChanged();
                System.out.println("请求成功");
            }

            @Override
            public void onFailure(Call<List<DinDanBean>> call, Throwable t) {
                System.out.println("请求失败");
            }
        });
    }

    private void requestwait() {
        Call<List<DinDanBean>> listCall = dinDanService.getDinDanWait();
        listCall.enqueue(new Callback<List<DinDanBean>>() {
            @Override
            public void onResponse(Call<List<DinDanBean>> call, Response<List<DinDanBean>> response) {
                dinDanBeanList = response.body();
                System.out.println(dinDanBeanList);
                myAdapter.notifyDataSetChanged();
                System.out.println("请求成功");
            }

            @Override
            public void onFailure(Call<List<DinDanBean>> call, Throwable t) {
                System.out.println("请求失败");
            }
        });
    }

    private void requestfinish() {
        Call<List<DinDanBean>> listCall = dinDanService.getDinDanFinish();
        listCall.enqueue(new Callback<List<DinDanBean>>() {
            @Override
            public void onResponse(Call<List<DinDanBean>> call, Response<List<DinDanBean>> response) {
                dinDanBeanList = response.body();
                System.out.println(dinDanBeanList);
                myAdapter.notifyDataSetChanged();
                System.out.println("请求成功");
            }

            @Override
            public void onFailure(Call<List<DinDanBean>> call, Throwable t) {
                System.out.println("请求失败");
            }
        });
    }
    private void initView() {
        for (int i = 0; i < arrayTab.length; i++) {
            xiaoXiBinding.mTabLayout.addTab(xiaoXiBinding.mTabLayout.newTab().setText(arrayTab[i]));
        }
        xiaoXiBinding.mTabLayout.selectTab(xiaoXiBinding.mTabLayout.getTabAt(0));
        xiaoXiBinding.mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                System.out.println("tab ============>" + tab.getPosition());
                if (tab.getPosition()==1)
                    requestwait();
                else if (tab.getPosition()==2)
                    requestfinish();
                else
                    request();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        DinDanBean dinDanBean = new DinDanBean();
        dinDanBean.shopName = "费德勒•欧德聚";
        dinDanBean.eatingStyle = "移动端送餐";
        dinDanBean.time = "2022-03-11  12:00";
        dinDanBean.diancan = "7";
        dinDanBean.price = "288";
        dinDanBean.judge = "1";
        dinDanBean.img = "https://cdn.jsdelivr.net/gh/Yqifei/img@master/image.70n8g8j8gyg0.webp";
        dinDanBeanList.add(dinDanBean);

    }

    private class MyAdapter extends RecyclerView.Adapter<MyViewHolderDingDan> {

        @NonNull
        @Override
        public MyViewHolderDingDan onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view1 = LayoutInflater.from(getContext()).inflate(R.layout.dingdan_status_item, parent, false);
            MyViewHolderDingDan myViewHolder = new MyViewHolderDingDan(view1);
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolderDingDan holder, int position) {
           /* if (position%3==1){
                holder.status_pay_tv.setText("已付款");
                holder.status_pay_tv.setTextColor(getResources().getColor(R.color.price_red));
                holder.quxiao_pay_bt.setText("订单回持");
                holder.status_pay_iv.setImageDrawable(getResources().getDrawable(R.drawable.hot_red));
                holder.shop_iv.setImageDrawable(getResources().getDrawable(R.drawable.canding2));
            }
            if (position%3==2){
                holder.status_pay_tv.setText("已完成");
                holder.status_pay_tv.setTextColor(getResources().getColor(R.color.blue_color));
                holder.quxiao_pay_bt.setVisibility(View.INVISIBLE);
                holder.zhifu_pay_bt.setText("立即评价");
                holder.status_pay_iv.setImageDrawable(getResources().getDrawable(R.drawable.hot_lan));
                holder.shop_iv.setImageDrawable(getResources().getDrawable(R.drawable.canding2));
            }*/
            DinDanBean danBean = dinDanBeanList.get(position);
            if (dinDanBeanList.get(position).judge.equals("1")){
                // 已付款 绿色
                holder.status_pay_tv.setText("已付款");
                holder.quxiao_pay_bt.setText("订单回持");
            }else if (dinDanBeanList.get(position).judge.equals("2")){
                // 已完成 蓝色
                holder.status_pay_tv.setText("已完成");
                holder.status_pay_tv.setTextColor(getResources().getColor(R.color.blue_color));
                holder.quxiao_pay_bt.setVisibility(View.INVISIBLE);
                holder.zhifu_pay_bt.setText("立即评价");
                holder.status_pay_iv.setImageDrawable(getResources().getDrawable(R.drawable.hot_lan));
                holder.shop_iv.setImageDrawable(getResources().getDrawable(R.drawable.canding2));
            }else if(dinDanBeanList.get(position).judge.equals("3")){
                //待付款 红色
                holder.status_pay_tv.setText("待付款");
                holder.status_pay_tv.setTextColor(getResources().getColor(R.color.price_red));
                holder.status_pay_iv.setImageDrawable(getResources().getDrawable(R.drawable.hot_red));
                holder.shop_iv.setImageDrawable(getResources().getDrawable(R.drawable.canding2));

                holder.zhifu_pay_bt.setText("立即支付");
            }
            Glide.with(getContext()).load(danBean.img).into(holder.shop_iv);
            holder.shop_name_tv.setText(danBean.shopName);
            holder.jiucan_style_tv.setText(danBean.eatingStyle);
            holder.time_tv.setText(danBean.time);
            holder.money_tv.setText("点餐"+danBean.diancan+"项，总计￥"+danBean.price+"元");

        }

        @Override
        public int getItemCount() {
            return dinDanBeanList.size();
        }
    }
}